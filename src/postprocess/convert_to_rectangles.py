import numpy as np
import cv2
import matplotlib.pyplot as plt
from scipy.ndimage import rotate
from skimage.morphology import convex_hull_image
from scipy.ndimage.measurements import center_of_mass
from scipy.ndimage.interpolation import shift
from tqdm import tqdm
from skimage.transform import resize

from src.postprocess.utils import get_random_probability


def rescale_array_from_centre_of_mass(array, rescale_factor=1.):
    if rescale_factor == 1:
        return array
    array = np.copy(array)
    assert rescale_factor >= 1
    if np.sum(array) == 0: return array
    original_shape = array.shape
    x_center, y_center = center_of_mass(array)
    shift_x = int(array.shape[0] / 2 - x_center)
    shift_y = int(array.shape[1] / 2 - y_center)
    array = shift(array, [shift_x, shift_y])
    array = resize(array, [int(array.shape[0] * rescale_factor),
                           int(array.shape[1] * rescale_factor)],
                   )
    array = shift(array, [int(-array.shape[0] // 2 + x_center), int(-array.shape[1] // 2 + y_center)])

    return array[:original_shape[0], :original_shape[1]]


class ProbabilityConverter:

    def __init__(self, converter_method=convex_hull_image):

        self.converter_method = converter_method

    #  cv2.minAreaRect

    def convert_propability_to_bbox(self, probability, threshold, min_size):
        """
        Post processing of each predicted mask, components with lesser number of pixels
        than `min_size` are ignored
        """
        # don't remember where I saw it
        mask = cv2.threshold(probability, threshold, 1, cv2.THRESH_BINARY)[1]
        num_component, mask_with_subsets = cv2.connectedComponents(mask.astype(np.uint8))
        predictions = self.transform_multiple_component_mask_to_bboxes(mask_with_subsets, min_size)

        return predictions, num_component - 1

    def convert_probability_to_multiple_bbox(self, probability, reverse=False):
        bboxes = np.zeros_like(probability)

        thresholds = np.linspace(0, 0.9, 10)
        for threshold in thresholds:
            bbox_at_threshold, _ = self.convert_propability_to_bbox(probability, threshold, min_size=0)
            if reverse:
                bboxes += bbox_at_threshold * (1 - threshold)
            else:
                bboxes += bbox_at_threshold * threshold

        return bboxes

    def convert_single_mask_to_bounding_box(self, mask):
        """
        Converts a binary mask (values in {0,1} to a bounding-box mask.
        The bounding box is determined as the minimum area rectangle which entails
        the mask
        """
        try:
            mask = self.converter_method(mask)
            mask = mask.astype(np.uint8)
            contours, hierarchy = cv2.findContours(mask, mode=1, method=2)
            cnt = contours[0]
            rect = cv2.minAreaRect(cnt)

            box_coordinates = cv2.boxPoints(rect)
            box_coordinates = np.ceil(box_coordinates.astype(np.uint16))
            box_coordinates.T[0] = np.clip(box_coordinates.T[0], 0, mask.shape[1])
            box_coordinates.T[1] = np.clip(box_coordinates.T[1], 0, mask.shape[0])
            box_coordinates = box_coordinates.astype(np.int32)
            x = np.zeros(mask.shape)
            x[box_coordinates.T[1], box_coordinates.T[0]] = 1
            return convex_hull_image(x) * 1
        except:
            return mask

    def transform_multiple_component_mask_to_bboxes(self, mask_with_subsets, min_size):
        predictions = np.zeros(mask_with_subsets.shape[:2], np.float32)
        num_component = np.max(mask_with_subsets) + 1
        for c in range(1, num_component):
            sub_mask = (mask_with_subsets == c)
            sub_mask = self.convert_single_mask_to_bounding_box(sub_mask)
            if sub_mask.sum() >= min_size:
                predictions += sub_mask
        predictions = np.clip(predictions, 0, 1)

        return predictions

    def post_process_with_scaling(self, probability, threshold, min_size, rescale_factor=1):
        """
        Post processing of each predicted mask, components with lesser number of pixels
        than `min_size` are ignored
        """
        # don't remember where I saw it
        mask = cv2.threshold(probability, threshold, 1, cv2.THRESH_BINARY)[1]
        num_component, component = cv2.connectedComponents(mask.astype(np.uint8))
        predictions = np.zeros((350, 525), np.float32)
        num = 0
        for c in range(1, num_component):
            p = (component == c)
            p = rescale_array_from_centre_of_mass(p, rescale_factor)
            if p.sum() > min_size:
                predictions[p] = 1
                num += 1
        return predictions, num


if __name__ == '__main__':
    converter = ProbabilityConverter()
    probability = get_random_probability()
    print(probability.shape)
    plt.imshow(probability)
    plt.show()
    mask, _ = converter.convert_propability_to_bbox(probability, threshold=0.5, min_size=0)
    plt.imshow(mask, cmap='gray')
    plt.show()

    mask = converter.convert_probability_to_multiple_bbox(probability)
    plt.imshow(mask, cmap='gray')
    plt.show()

    mask = converter.convert_probability_to_multiple_bbox(probability, reverse=True)
    plt.imshow(mask, cmap='gray')
    plt.show()

    import time

    t_0 = time.time()
    for _ in tqdm(range(10)):
        probability, xedges, yedges = np.histogram2d(np.random.randn(4096), np.random.randn(4096), bins=(128, 128))
        probability = rotate(probability, angle=45)
        probability = probability / probability.max()
        mask = converter.convert_probability_to_multiple_bbox(probability, reverse=True)
        # 154.89470410346985
    print(time.time() - t_0)
    probability = np.array([[np.exp(- 0.02 * (x_1 ** 2 + 5 * x_2 ** 2)) for x_1 in np.linspace(-10, 10, num=50)]
                            for x_2 in np.linspace(-10, 10, num=50)])
    probability += np.array([[np.exp(- 0.2 * (x_1 ** 2 + 5 * x_2 ** 2)) for x_1 in np.linspace(-5, 15, num=50)]
                             for x_2 in np.linspace(-5, 15, num=50)])
    probability = rotate(probability, angle=45)
    plt.imshow(probability)
    x = rescale_array_from_centre_of_mass(probability, 1.5)
    plt.imshow(x)
    plt.show()
