import numpy as np
from scipy.ndimage import rotate


def get_random_probability():
    probability = np.array([[np.exp(- 0.02 * (x_1 ** 2 + 5 * x_2 ** 2)) for x_1 in np.linspace(-10, 10, num=50)]
                            for x_2 in np.linspace(-10, 10, num=50)])
    probability += np.array([[np.exp(- 0.2 * (x_1 ** 2 + 5 * x_2 ** 2)) for x_1 in np.linspace(-5, 15, num=50)]
                             for x_2 in np.linspace(-5, 15, num=50)])
    probability = rotate(probability, angle=45)
    return probability


