from typing import List

from tqdm import tqdm

import torch

from src.utils.nn_utils import gpu_unravel


class TTA:

    def __init__(self, tta_forward, tta_backward=None, add_sigmoid=True):
        self.tta_forward = tta_forward  # torch on batch (bs, channels, w, h)
        self.tta_backward = tta_forward if tta_backward is None else tta_backward
        self.add_sigmoid = add_sigmoid

    def eval_model(self,
                   dataloader,
                   model,
                   batch_unravel_fn=None,
                   verbose=False):
        all_preds = None
        model.eval()
        for step, batch in tqdm(enumerate(dataloader), disable=1 - verbose):
            x, y = batch_unravel_fn(batch)
            with torch.no_grad():
                y_pred = model(x).detach().cpu()
            if all_preds is None:
                all_preds = y_pred
            else:
                all_preds = torch.cat([all_preds, y_pred], dim=0)
        return all_preds

    def predict(self, dataloader, model, batch_unravel_fn):
        def tta_unravel(batch):
            batch = [self.tta_forward(batch[0]), batch[1]]
            return batch_unravel_fn(batch)

        predictions = self.eval_model(dataloader, model, tta_unravel, verbose=True)
        if self.add_sigmoid:
            predictions = torch.sigmoid(predictions)
        return self.tta_backward(predictions).numpy()


def tta_predict(tta_methods: List[TTA], model, loader):
    fn2mask = {}
    for tta_method in tta_methods:
        mask_preds = tta_method.predict(loader, model, batch_unravel_fn=gpu_unravel)
        for mask_pred, image_filename in zip(mask_preds, loader.dataset.image_filenames):
            if fn2mask.get(image_filename) is None:
                fn2mask[image_filename] = torch.sigmoid(mask_pred).numpy() / len(tta_methods)
            else:
                fn2mask[image_filename] = fn2mask[image_filename] + torch.sigmoid(mask_pred).numpy() / len(tta_methods)
    return fn2mask


def torch_flip_h(x):
    return torch.flip(x, dims=[2])


def torch_flip_w(x):
    return torch.flip(x, dims=[3])


def torch_flipxy(x):
    return x.transpose(2, 3)


def torch_rot270(x):
    return x.transpose(2, 3).flip(3)


def torch_unrot270(x):
    return torch_flip_h(torch_flipxy(x))


tta_methods = [TTA(lambda x: x),
               TTA(torch_flip_h),
               TTA(torch_flip_w),
               TTA(torch_flipxy, torch_flipxy),
               TTA(torch_rot270, torch_unrot270)]
