from functools import lru_cache

import numpy as np

from src.postprocess.utils import get_random_probability


def convert_image_to_bbox(array):
    if np.sum(array) == 0: return array
    x_stats, y_stats = np.argwhere(array != 0).T
    bbox_array = np.zeros_like(array)
    bbox_array[x_stats.min(): x_stats.max() + 1, y_stats.min(): y_stats.max() + 1] = 1
    return bbox_array


class LocalBoundingBoxConverter2:

    @lru_cache(maxsize=10)
    def get_masks(self, array_flattened: tuple, array_shape, size_x, size_y):
        array = np.array(array_flattened).reshape(array_shape)
        num_x = array.shape[0] // size_x + 1
        num_y = array.shape[1] // size_y + 1
        mask = np.zeros((num_x, num_y) + array.shape, dtype=array.dtype)
        for i in range(num_x):
            for j in range(num_y):
                sub_mask = np.zeros_like(array)
                sub_mask[i * size_x: (i + 1) * size_x, j * size_y: (j + 1) * size_y] = 1
                mask[i, j] = sub_mask
        return mask.reshape((-1,) + array.shape)

    def convert_to_bounding_box(self, array: np.ndarray, size_x=10, size_y=10):
        if size_x == 0 or size_y == 0:
            return array

        bbox_array = np.zeros_like(array)
        masks = self.get_masks(tuple(array.flatten()), array.shape, size_x, size_y)
        for mask in masks:
            bbox_array += convert_image_to_bbox(mask * array)
        return bbox_array


def test_bbox_creation():
    probability = get_random_probability()
    probability = (probability > 0.5) * 1
    bbox_converter = LocalBoundingBoxConverter2()
    mask = bbox_converter.get_masks(tuple(probability.flatten()), probability.shape, size_x=10, size_y=10)
    assert np.sum(np.sum(mask, axis=0) - 1) == 0


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    probability = get_random_probability()
    probability = (probability > 0.5) * 1
    print(probability.shape)
    plt.imshow(probability)
    plt.show()
    plt.imshow(convert_image_to_bbox(probability))
    plt.show()
    bbox_converter = LocalBoundingBoxConverter2()
    bbox_prob = bbox_converter.convert_to_bounding_box(probability, size_x=10, size_y=10)
    plt.imshow(bbox_prob)
    plt.show()
