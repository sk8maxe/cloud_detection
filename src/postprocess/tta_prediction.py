import gc
import os
from typing import List

import numpy as np
import torch
from torch.utils.data import DataLoader

from src.dataloaders.augmentations import get_validation_augmentation
from src.dataloaders.cloud_dataset import CloudDataset
from src.postprocess.tta import TTA
from src.utils.nn_utils import gpu_unravel


def split(list_, num_elements=200):
    for i in range(0, len(list_), num_elements):
        yield list_[i:i + num_elements]


def update_filename2mask(filename2mask, filename2mask_new, normalization: float):
    for filename, mask in filename2mask_new.items():
        if filename2mask.get(filename) is None:
            filename2mask[filename] = mask / normalization
        else:
            filename2mask[filename] = filename2mask[filename] + mask / normalization


class TTAPredict:

    def __init__(self, model_initializers, tta_methods: List[TTA], df, image_path, save_filepath, batch_size):
        self.model_initializers = model_initializers
        self.tta_methods = tta_methods
        self.df = df
        self.image_path = image_path
        self.save_filepath = save_filepath
        self.batch_size = batch_size

    def _get_loader(self, filenames, preprocessing):
        num_workers = 1
        dataset = CloudDataset(df=self.df,
                               data_folder=self.image_path,
                               image_filenames=filenames,
                               transforms=get_validation_augmentation(),
                               preprocessing=preprocessing)
        loader = DataLoader(dataset, batch_size=self.batch_size, shuffle=False, num_workers=num_workers)
        return loader

    def _tta_predict(self, tta_methods: List[TTA], model, loader):
        filename2mask = {}
        for tta_method in tta_methods:
            mask_preds = tta_method.predict(loader, model, batch_unravel_fn=gpu_unravel)
            update_filename2mask(filename2mask,
                                 dict(zip(loader.dataset.image_filenames, mask_preds)),
                                 len(tta_methods))
        return filename2mask

    def _predict_one_fold(self, fold_idx, filenames):
        filename2mask = {}
        for model_initializer in self.model_initializers:
            model, preprocessing = model_initializer(fold_idx)
            loader = self._get_loader(filenames, preprocessing)
            filename2mask_single_model = self._tta_predict(self.tta_methods, model, loader)
            update_filename2mask(filename2mask, filename2mask_single_model, normalization=len(self.model_initializers))
            del model
            gc.collect()
            torch.cuda.empty_cache()

        return filename2mask

    def _save_imgs(self, filename2mask):
        for filename, mask in filename2mask.items():
            np.savez_compressed(os.path.join(self.save_filepath, filename),
                                (mask * 255).astype(np.uint8))

    def _remove_images_already_processed(self, filenames):
        num_files = len(filenames)
        filenames = [filename for filename in filenames
                     if not os.path.exists(os.path.join(self.save_filepath, filename))]
        print(f'Removed {len(filenames) - num_files}, since they are already saved.')
        return filenames

    def predict(self,
                filenames,
                fold_idxs=(0,),
                images_per_split=200,
                debug=False):
        filenames = self._remove_images_already_processed(filenames)
        for filenames_subset in split(filenames, num_elements=images_per_split):

            filename2mask = {}
            for fold_idx in fold_idxs:
                filename2mask_single_fold = self._predict_one_fold(fold_idx, filenames_subset)
                update_filename2mask(filename2mask, filename2mask_single_fold, len(fold_idxs))
            if debug:
                return filename2mask
            self._save_imgs(filename2mask)
