import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def dice(y_true, y_pred, from_logits=True):
    if from_logits:
        func = sigmoid
    else:
        func = lambda x: x

    eps = 1e-7,
    intersection = np.sum(y_true * func(y_pred))
    union = np.sum(y_true) + np.sum(func(y_pred))
    dice = 2 * intersection / (union + eps)

    return dice[0]
