import os

import cv2
import pandas as pd

from torch.utils.data import Dataset
import numpy as np
import albumentations as albu
from albumentations import pytorch as AT

from src.utils.rle_utils import make_mask


class CloudDataset(Dataset):
    def __init__(self,
                 df: pd.DataFrame,
                 data_folder: str,
                 image_filenames: np.array,
                 transforms=albu.Compose([albu.HorizontalFlip(), AT.ToTensorV2()]),
                 preprocessing=None):
        self.df = df
        self.data_folder = data_folder
        self.image_filenames = image_filenames
        self.transforms = transforms
        self.preprocessing = preprocessing

    def __getitem__(self, idx):
        image_name = self.image_filenames[idx]
        mask = make_mask(self.df, image_name)
        image_path = os.path.join(self.data_folder, image_name)
        img = cv2.imread(image_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        augmented = self.transforms(image=img, mask=mask)
        img = augmented['image']
        mask = augmented['mask']
        if self.preprocessing:
            preprocessed = self.preprocessing(image=img, mask=mask)
            img = preprocessed['image']
            mask = preprocessed['mask']
        return img, mask

    def __len__(self):
        return len(self.image_filenames)
