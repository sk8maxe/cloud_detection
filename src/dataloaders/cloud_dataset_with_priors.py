import os
import pandas as pd
import numpy as np
import albumentations as albu
from albumentations import pytorch as AT

from src.dataloaders.cloud_dataset import CloudDataset


class CloudDatasetWithPriors(CloudDataset):

    def __init__(self,
                 df: pd.DataFrame,
                 data_folder: str,
                 priors_folder: str,
                 image_filenames: np.array,
                 transforms=albu.Compose([albu.HorizontalFlip(), AT.ToTensorV2()]),
                 preprocessing=None,
                 ):
        super().__init__(df, data_folder, image_filenames, transforms, preprocessing)
        self.priors_folder = priors_folder

    def __getitem__(self, idx):
        img, mask = super().__getitem__(idx)
        image_name = self.image_filenames[idx] + '.npz'
        image_path = os.path.join(self.priors_folder, image_name)
        with np.load(image_path) as data:
            predicted_mask = (data['arr_0'] / 255).astype(np.float32)
        img = np.concatenate([img, predicted_mask], axis=0)
        return img, mask

    def __len__(self):
        return len(self.image_filenames)
