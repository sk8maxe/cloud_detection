import random

import torch
from tqdm import tqdm
import time
import os
import numpy as np


def seed_everything(seed=1234):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True


def eval(val_dl, model, loss_fn=None, return_logits=False, metric_fns={}, verbose=False, batch_unravel_fn=None):
    val_hist = {'metrics': {m: 0 for m in metric_fns},
                'val_loss': 0,
                'logits': None}

    # batch wise or total loss
    all_logits = []
    all_labels = []

    model.eval()
    eval_loss = 0
    for step, batch in tqdm(enumerate(val_dl), disable=1 - verbose):

        if batch_unravel_fn is None:
            batch = tuple(t.cuda() for t in batch)
            x, y = batch
        else:
            x, y = batch_unravel_fn(batch)

        with torch.no_grad():

            logits = model(x)
        if loss_fn is not None:
            tmp_eval_loss = loss_fn(logits, y)
            eval_loss += tmp_eval_loss.mean().item()

        all_logits += [logits.detach().cpu().numpy()]
        all_labels += [y.detach().cpu().numpy()]

    all_logits = np.concatenate(all_logits)
    all_labels = np.concatenate(all_labels)

    eval_loss = eval_loss / len(val_dl)

    val_hist['val_loss'] = eval_loss

    for fn in metric_fns:
        val_hist['metrics'][fn] = metric_fns[fn](all_labels, all_logits)

    if return_logits:
        val_hist['logits'] = all_logits

    return val_hist


def lazy_eval(val_dl, model, loss_fn=None, metric_fns={}, verbose=False, batch_unravel_fn=None, postprocess_fn=None):
    val_hist = {'metrics': {m: 0 for m in metric_fns},
                'val_loss': 0,
                'logits': None}

    model.eval()
    eval_loss = 0
    for step, batch in tqdm(enumerate(val_dl), disable=1 - verbose):

        if batch_unravel_fn is None:
            batch = tuple(t.cuda() for t in batch)
            x, y = batch
        else:
            x, y = batch_unravel_fn(batch)

        with torch.no_grad():
            y_preds = model(x)

        if loss_fn is not None:
            tmp_eval_loss = loss_fn(y_preds, y)
            eval_loss += tmp_eval_loss.mean().item()

        if postprocess_fn is not None:
            postprocess_fn(x, y, y_preds, step)
    eval_loss = eval_loss / len(val_dl)

    val_hist['val_loss'] = eval_loss

    return val_hist


def fit(model, train_dl, optimizer, loss_fn, epochs=1, val_dl=None, verbose=False, scheduler=None, metric_fns={},
        gradient_accumulation_steps=1, fp16=False, callbacks=None, hist=None, batch_unravel_fn=None,
        valid_unravel_fn=None):
    if hist is None:
        hist = {'metrics': {m: [] for m in metric_fns}, 'tr_loss': [], 'val_loss': [], 'tr_time': [], 'val_time': [],
                'lr': []}
    if valid_unravel_fn is None:
        valid_unravel_fn = batch_unravel_fn
    for epoch in range(epochs):

        start_time = time.time()

        tr_loss = 0
        model.train()
        tqdm_loader = tqdm(enumerate(train_dl), total=len(train_dl), disable=1 - verbose)
        for step, batch in tqdm_loader:

            if batch_unravel_fn is None:
                batch = tuple(t.cuda() for t in batch)
                x, y = batch
            else:
                x, y = batch_unravel_fn(batch)

            logits = model(x)
            loss = loss_fn(logits, y)

            # if args['gradient_accumulation_steps'] > 1:
            #     loss = loss / args['gradient_accumulation_steps']

            if fp16:
                optimizer.backward(loss)
            else:
                loss.backward()
            tr_loss += loss.item()
            tqdm_loader.set_description(f"Loss: {tr_loss / (step + 1)}")

            if (step + 1) % gradient_accumulation_steps == 0:
                optimizer.step()
                optimizer.zero_grad()
        if scheduler is not None:
            scheduler.step()
            # print(scheduler.get_lr())
            hist['lr'] = scheduler.get_lr()
        tr_loss = tr_loss / len(train_dl)
        tr_time = time.time() - start_time

        hist['tr_loss'] += [tr_loss]
        hist['tr_time'] += [tr_time]

        # EVAL
        if val_dl is not None:
            val_start_time = time.time()
            val_hist = eval(val_dl, model, loss_fn, metric_fns=metric_fns, batch_unravel_fn=valid_unravel_fn)

            val_time = time.time() - val_start_time

            hist['val_loss'] += [val_hist['val_loss']]
            hist['val_time'] += [val_time]
            for m in val_hist['metrics']:
                hist['metrics'][m] += [val_hist['metrics'][m]]

        # apply callbacks
        for callback in callbacks:
            callback(epoch, hist, model)

    return hist


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def cpu_unravel(batch):
    return batch[0], batch[1]


def gpu_unravel(batch):
    return batch[0].cuda(), batch[1].cuda()
