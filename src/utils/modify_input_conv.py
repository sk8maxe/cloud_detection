from efficientnet_pytorch.utils import get_same_padding_conv2d


def modify_input_conv_efficientnet(model, in_channels, image_size=[224, 224]):

    Conv2d = get_same_padding_conv2d(image_size=image_size)
    params = model.encoder._conv_stem.__dict__
    new_conv = Conv2d(in_channels,
                      out_channels=params['out_channels'],
                      kernel_size=3,
                      stride=2,
                      bias=False)

    model.encoder._conv_stem = new_conv


if __name__ == '__main__':
    import segmentation_models_pytorch as smp
    model = smp.UnetPlus(
        encoder_name='efficientnet-b2',
        encoder_weights=None,
        classes=4,
        activation=None,
    )
    modify_input_conv_efficientnet(model, in_channels=7)
