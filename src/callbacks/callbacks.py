import os

import numpy as np
import torch

import math

inf = math.inf
nan = math.nan


class ModelCheckpoint:

    def __init__(self, save_path, verbose=True, save_best_only=True, save_epochs=None):
        if save_epochs is None:
            save_epochs = []

        self.save_path = save_path
        save_dir = os.path.dirname(os.path.abspath(save_path))
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        self.verbose = verbose
        self.save_best_only = save_best_only
        self.save_epochs = save_epochs

    def __call__(self, epoch, hist, model):
        if self.save_best_only:
            if np.argmin(hist['val_loss']) == len(hist['val_loss']) - 1:
                torch.save(model.state_dict(), self.save_path)
                if self.verbose:
                    print('saved')
        if len(self.save_epochs) > 0:
            if epoch in self.save_epochs:
                torch.save(model.state_dict(),
                           ''.join(self.save_path.split('.')[:-1]) + f'_e{epoch}.' + self.save_path.split('.')[-1])
                if self.verbose:
                    print('saved')


class PrinterCallback:

    def __init__(self):
        pass

    def __call__(self, epoch, hist, model):
        tr_loss = hist['tr_loss'][-1]
        val_loss = hist['val_loss'][-1]
        tr_time = hist['tr_time'][-1]
        val_time = hist['val_time'][-1]
        if val_loss is not None:
            overfit_ratio = (val_loss - tr_loss) / abs(tr_loss)
            msg = f'Epoch: {epoch:d} | loss: {tr_loss:.3f} | val_loss: {val_loss:.3f} | ofr: {overfit_ratio:.2%} | time: {tr_time + val_time:.3f}s'
            for metric in hist['metrics']:
                msg += f' | {metric}: {hist["metrics"][metric][-1]:.3f}'
        else:
            msg = f'Epoch: {epoch:d} | loss: {tr_loss:.3f} | time: {tr_time:.3f}s'
        print(msg)
