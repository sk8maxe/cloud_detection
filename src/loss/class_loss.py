import torch


def class_loss(loss_function, ratios=(1, 1, 1, 1)):
    def forward(y_true, y_pred):
        # (bs, num_classes, width, height)
        loss = 0
        for i in range(y_true.shape[1]):
            loss += loss_function(y_true[:, i:i + 1], y_pred[:, i: i + 1]) * ratios[i]
        return loss / len(y_true.shape[1])

    return forward
